*This document is a draft*

# The CO_OP

The purpose of the cooperative is to support **more people doing work that matters** in a way that respects our shared values.

## Values

- **Curiosity**, Learning, Exploration, Creativity, Growth
- **Justice**, Anti-colonialism, Anti-oppression, Intersectionality
- **Honesty**, Moral integrity, Authenticity
- **Minimizing Waste**, Earth care, Doing it right the first time, Good work
- **Cooperations**, Sharing, Hosting, Generosity, Passing knowledge
- **People care**, Acknowledgment, Gratitude, Fair & compassionate compensation, Flexible schedule, room for human-ness

## Metrics

We measure our success by asking reflective questions, these represent our goals:

- Do contributors feel fulfilled? Are contributors fulfilled?
- Is the work good?
- Do people feel invited?
- Are clients/patrons/partners happy?
- Are we shifting the culture?
- Are we being adventurous?
- Are we attending to organizational well-being, inc. financial, clear sense of purpose, focus?

## Membranes & Membership

The cooperative aims to host individuals so they can be the best possible version of themselves while doing good work in the world. We are able to do this by offering autonomy, benefits, ownership, and responsibilities based on the individual's level of commitment.

There are 4 basic levels of membership in the co-op:

1. **Member Owner** - Co-owns and is ultimately responsible for the co-op.
    - **Acting Member** - Contributor who has been voted in to be a member but has something left to fulfill (for instance paying member buy in)
    - **Rising Member** - Contributor who's membership agreement has been accepted and is now on membership track
2. **Contributor** - Works for the co-op and has stake by way of retainer or salary.
3. **Contractor** - Works for the co-op on an hourly basis.
4. **Alumni** - Former member owner

### Member Owner

Members are the owners and stewards of the co-op. They have the most "skin in the game" and hold the most responsibility for the co-op being healthy and productive. They stand to benefit from the excess yield produced by the co-op.

Benefits                     | Agrees too
---------------------------- | ----------------------------
Salary                       | Buy 1 Common Stock (vote)
Distributions                | Attend membership meetings
Patronage                    | Hold a position on the board
Healthcare                   | Work `16` days per month
Hardware budget              | + Contributor agreements
Software accounts            | 
Personal development stipend | 

**Requirements to become Member Owner**

Article 1 Section 1.b of the co-op bylaws.

1. Creates membership agreement with metrics for acceptance
2. Member Owner agrees to steward contributor
3. Membership agreement is accepted by the board
4. Membership agreement milestones are met
5. Purchases 1 common stock for `$3,000` or create plan to pay
6. Be voted in by the board

**Removal of Member Owner**

Article 1 Section 3.b of the co-op bylaws.

- Fails to uphold Membership Agreement for a consecutive period of two months
- Violates provision of the Membership Agreement or any other policy or procedures of the Cooperative
- dies
- otherwise ceased to be eligible for membership in the Cooperative;
- Is terminated by the board

### Contributor

All member owners are also Contributors. The primary difference is that Contributors have less stake (and thus hold less risk) in the co-op. They also can participate in the patronage system.

Benefits                     | Agrees to
---------------------------- | ----------------------------
Salary / retainer option     | Take responsibility for generating income 
Patronage option             | Perform administrative duties
Software accounts            | Be accountable to the co-op
Personal development stipend |

**Requirements to become Contributor**

1. Is invited by two members
2. Creates contributor agreement with the co-op

**Removal of Contributor**

- Mutual dissolving of agreement
- Terminated by the board

### Contractor

The co-op hires contractors to do work.

Benefits                     | Agrees to
---------------------------- | ----------------------------
Respect                      | Do the work
Fair compensation            | Treat one's self and others with respect

**Becoming a contractor**

1. Member hires for project
2. Agrees to scope of work

**Removal of contractor**

- Mutual dissolving of agreement

## Working Days

Contributors on salary/retainer and members track their activity by the day. There are four status for tracking a day which each has a point value associated with it:

- `0` Personal day - non-working day
- `0.5` Half day - working for half a day or about 4 hours
- `1` Full day - works a full day
- `1.2` Long day - works over a full day, this number needs to reward extra work while disincentivizing over working.

The tracking of working days factors into **Contributor Remuneration** and **Patronage**.

*Why?* We wanted to get away from tracking hours! Furthermore this system also allows us to estimate our availability and project capacity within the co-op.

## Patronage

Contributors on salary/retainer and members earn patronage by working "days" for the co-op. At the end of each year the the total "days" worked are added up for each contributor and the co-op's surplus (as determined by the board) is distributed based on the ratio of all contributor's patronage.

Example: Drew, Katie, and Rowan have worked `420`, `460`, and `240` total days respectively over the past 2 years. At the end of the 2nd year the board decides that `$6,000` of surplus was generated. The surplus is then distributed like so:

- Drew's `420` hours is `40.38%` of surplus is `$2,423.08`
- Katie's `460` hours is `44.23%` of surplus is `$2,653.85`
- Rowan's `160` hours is `15.38%` of surplus is `$923.08`

Because Katie and Drew have been working longer for the co-op they get a larger cut.

*Why?* We want to share in the co-op's yield and reward people who have invested more in the co-op.

## Compensation


### Fixed Price Projects

For fixed price projects `20%` is taken off the top for overhead. The overhead is split into:

- `50%` for finders fee
- `40%` for project management
- `10%` for Statement of Work / Proposal

Example: Katie brings in a `$17,000` project, writes the SOW, and gets Jen to do project management. So `20%` of `$17,000` is `$3,400`. Of that:

- `50% $1,700` goes to Katie as a finders fee
- `40% $1,360` goes to Jen for project management
- `10% $340` goes to Katie for the SOW

The remaining `$13,600` is used to pay contractors with anything remaining going to the co-op as profit.

#### Project Buy

On fixed price projects contractors "buy" part of the project contract and agree to meet Statement of Work for a fixed price.

Example: Katie brings in a project for `$4,600`. `$920 (20%)` overhead is taken off the top. Drew writes the SOW and does the project management. Eric agrees to do the design portion of the project for `$2,000`. Julianna also does 5 hours of work at `$65`per hour for the project.

Overhead (`$920`) breaks down like so:

- `50% $460` goes to Katie as a finders fee
- `40% $368` goes to Drew for project management
- `10% $92` goes to Drew for the SOW

The remaining `$3,680` is split like so:

- `$2,000` goes to Eric for design work
- `$325` goes to Julianna for her hourly work
- `$1,355` goes to the co-op as profit

*Why?* Tracking hours sucks and contractors have told us in the past that doing work with an hour limit is stressful. It also allows the co-op to have a better sense of where money is going.

### Hourly Contracts

Often we will have contracts with clients that bill hourly. The co-op has a standard rate of `$110` per hour. Workers have their own rate while contributors have a commission percentage. All hours of work for the client are tracked and billed at the co-op rate. Contractors are paid out at their personal rate with the difference going to the co-op as profit. Contributors earn a commission on hourly work.

Example: The co-op has a client who is billed at the co-op rate of `$110` per hour. This month Rowan (a contributor), Drew (a member), and Noëlle (a contractor) work a number of hours on the project. The rate and commission for each is:

- Rowan earns a `20%` commission
- Drew earns a `30%` commission
- Noëlle has a rate of `$65` per hour

If each work 10 hours on the project:

- Rowan earns `10 * .2 * $110 = $220` with `$880` going to the co-op as profit
- Drew earns `10 * .3 * $110 = $330` with `$770` going to the co-op as profit
- Noëlle earns `10 * $65 = $650` with `$450` going to the co-op as profit

For contributors/members this represents a "performance bonus" calculated as `Hours worked * commission * co-op rate`.

*Why?* We want to reward contributors for generating income. Most of that income flows into the co-op (and thus the contributor salary). Billing clients at the co-op rate while paying contractors out at their preferred rate allows us to generate income on these hours which provides a buffer if we need to pay contractors for more hours (or pay them for internal work).

### Contributor Remuneration

For contributors and members we offer a method for having a regular income that scales up as the co-op does better.

**Base Income + Business Viability Bonus + Performance Bonus**

- **Base Income** = Member/Contributor's base salary/retainer
- **Business Viability Bonus** = days worked &times; buffer bonus (day rate)
- **Performance Bonus** = Hours worked &times; commission &times; co-op rate

### Buffer

The buffer represents how much money we have in the bank. Until we have a separate account this number represents the "floor" of our primary bank account. Currently the floor is `$0`, we do not allow our account drop below 0, as we earn more money we raise the floor. Using money in the buffer requires a decision by the board.

For instance if our buffer is `$3,000` then using the last `$3,000` in the bank account requires board acceptance.

#### Buffer Bonus

For the **Business Viability Bonus** in the **Contributor Remuneration** a "buffer bonus" is multiplied by the total days worked by a contributor. This bonus is calculated based on how much money is available in the buffer.

A unit of buffer is the number of people on salary in the co-op multiplied by `$4,000`.

- Level 0 = We have less than 1 month worth of the combined members buffer.
- Level 1 = We have 1 month worth of the combined members buffer.
- Level 2 = We have 2 months worth of the combined members buffer.
- Level 3 = We have 3 months worth of the combined members buffer.
- Level 4 = We have 4 months worth of the combined members buffer.

| Buffer Level | Day Rate |
| ---          | ---      |
| Level 0      | `$100`   |
| Level 1      | `$150`   |
| Level 2      | `$200`   |
| Level 3      | `$250`   |
| Level 4      | `$300`   |

Example: If we have `3` salaried contributors then each unit of buffer is `3 * $4,000 = $12,000`. So if our buffer account contains `$26,000` that represents a little over 2 months worth of buffer which set's our buffer level to 2 and the day rate multiplier to `$200`.

### Example






